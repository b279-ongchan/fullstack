const express = require("express");
const mongoose = require("mongoose");
// Allows our backend application to be available to our frontend application
// Allows us to control the app's Cross Origin Resource Sharing settings
const cors = require("cors");
// Allows access to routes defined within our application
const userRoutes = require("./routes/user");
const courseRoutes = require("./routes/course");

// Creates an "app" variable that stores the result of the "express" function that initializes our express application and allows us access to different methods that will make backend creation easy
const app = express();

// Connect to our MongoDB database
mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.sk2eror.mongodb.net/Course_Booking_Application?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	});
// Prompts a message in the terminal once the connection is "open" and we are able to successfully connect to our database
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'));

// Allows all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" string to be included for all user routes defined in the "user" route file
app.use("/users", userRoutes);
// Defines the "/courses" string to be included for all course routes defined in the "course" route file
app.use("/courses", courseRoutes);

// Will used the defined port number for the application whenever an environment variable is available OR will used port 4000 if none is defined
// This syntax will allow flexibility when using the application locally or as a hosted application
app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
});