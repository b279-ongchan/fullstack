import React from "react";

// create a context object 
const UserContext = React.createContext();

// The Provider component allows other components to consume/use the context
export const UserProvider = UserContext.Provider;

export default UserContext;