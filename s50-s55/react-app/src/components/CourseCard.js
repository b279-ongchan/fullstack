import { Card, Button } from 'react-bootstrap';


// in react.js we have 3 hooks
/*
1. useState - more on mathematical operations
2. useEffect - 
3. useContext - admin or not


*/ 

import {useState} from "react"
import {Link} from "react-router-dom"

export default function CourseCard({courseProp}) {
	// checks if prop was successfully passed
	console.log(courseProp.name);
	// checks the type of passed data
	console.log(typeof courseProp);

	// destructing the courseProp into their own variables
	const{_id, name, description, price}=courseProp

/*	// useState
	// used for storing different states/ status
	// used to keep track of information to individual components

	// SYNTAX -> const[getter, setter] = useState(initialGetterValue)

	const [count, setCount] = useState(0);
	console.log(useState(0));
	const [seat, setSeat]=useState(30);

	// Function that keeps track of the enrollees for a course
	    function enroll(){
        if(seat > 0){
            setCount(count + 1);
            setSeat(seat - 1);
        }else{
            alert("No more seats available!");
        }
        
        console.log("Enrollees: " + count);
        console.log("Available Seats: " + seat);
    }
	
*/

    return (
        <Card className="my-3">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Link className="btn btn-primary" to={`/courseView/${_id}`}>Details</Link>


{/*                <Card.Subtitle>Enrollees:</Card.Subtitle>
                <Card.Text>{count}</Card.Text>
                <Button variant="primary" onClick={enroll}>Enroll</Button> */}
            </Card.Body>
        </Card>
    )
}
